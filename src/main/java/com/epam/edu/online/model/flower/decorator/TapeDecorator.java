package com.epam.edu.online.model.flower.decorator;

import com.epam.edu.online.model.flower.Flower;

public class TapeDecorator extends FlowerDecorator {

    private Double length;
    private final double ADDITIONAL_PRICE = 5;

    public TapeDecorator(Flower flower, Double length) {
        super(flower);
        this.length = length==0?1:length;
    }

    @Override
    public double getCost() {
        return super.getCost()+ getTapeCost();
    }

    @Override
    public String getName() {
        return super.getName() + " + " + this.length + "m. tape";
    }

    private double getTapeCost(){
        return length * ADDITIONAL_PRICE;
    }


    @Override
    public String toString() {
        return super.toString() +
                "length=" + length +
                ", ADDITIONAL_PRICE=" + ADDITIONAL_PRICE;
    }
}
