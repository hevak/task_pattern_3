package com.epam.edu.online.model.flower.decorator;

import com.epam.edu.online.model.flower.Event;
import com.epam.edu.online.model.flower.Flower;

import java.util.List;

public class FlowerDecorator implements Flower {
    private Flower flower;
    private int count;

    protected FlowerDecorator(Flower flower) {
        super();
        this.flower = flower;
    }

    @Override
    public List<Event> getType() {
        return flower.getType();
    }

    @Override
    public String getName() {
        return flower.getName();
    }

    @Override
    public double getCost() {
        return flower.getCost();
    }

    @Override
    public void setCount(int count) {
        if (count > 0) {
            this.count = count;
        }
    }

    @Override
    public String toString() {
        return flower +", count=" + count;
    }
}
