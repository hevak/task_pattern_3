package com.epam.edu.online.model.client;

public interface Subscriber {
    void inform(String message);
    String getName();
}
