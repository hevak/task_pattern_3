package com.epam.edu.online.model.flower.decorator;

import com.epam.edu.online.model.flower.Flower;

public class DiscontDecorator extends FlowerDecorator {

    private double discount;

    public DiscontDecorator(Flower flower, double discount) {
        super(flower);
        this.discount = discount<=0||discount>=100?0:discount;
    }

    @Override
    public double getCost() {
        return super.getCost() * getDiscount();
    }

    @Override
    public String getName() {
        return super.getName() + " + " + this.discount + "% of discount";
    }

    private double getDiscount(){
        return 1 - (discount / 100);
    }

    @Override
    public String toString() {
        return super.toString()+
                "discount=" + discount;
    }
}
