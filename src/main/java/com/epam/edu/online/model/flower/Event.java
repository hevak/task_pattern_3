package com.epam.edu.online.model.flower;

public enum Event {
    WEDDING, BIRTHDAY, FUNERAL, VALENTINES_DAY
}
