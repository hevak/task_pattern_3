package com.epam.edu.online.model.flower;

import java.util.List;

public interface Flower {

    List<Event> getType();
    String getName();
    double getCost();
    void setCount(int count);

}
