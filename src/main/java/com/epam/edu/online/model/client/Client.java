package com.epam.edu.online.model.client;

import com.epam.edu.online.model.flower.FlowerShop;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Client implements Subscriber {
    private static final Logger log = LogManager.getLogger(Client.class);

    private String name;
    private int discontPercentage;

    public Client(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDiscontPercentage() {
        return discontPercentage;
    }

    public void setDiscontPercentage(int discontPercentage) {
        if (discontPercentage >= 0 && discontPercentage <= 100) {
            this.discontPercentage = discontPercentage;
            log.info("seted " + discontPercentage + "% discount");
        } else {
            log.warn(discontPercentage + "% discount not allowed");
        }
    }

    public FlowerShop createOrder(FlowerShop shop){
        return shop;
    }


    @Override
    public void inform(String message) {
        log.info("Hello " + this.getName()+"!\n" + message);
    }


    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", discontPercentage=" + discontPercentage +
                '}';
    }
}
