package com.epam.edu.online.model.flower;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Rose implements Flower{
    private final String NAME = "Rose";
    private double COST = 21.5;
    private int count;
    private List<Event> type;

    public Rose() {
        type = new ArrayList<>(Arrays.asList(Event.BIRTHDAY, Event.FUNERAL, Event.VALENTINES_DAY));
    }

    public void setCount(int count) {
        if (count > 0) {
            this.count = count;
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public List<Event> getType() {
        return type;
    }

    @Override
    public double getCost() {
        return COST * (count == 0 ? 1 : count);
    }

    @Override
    public String toString() {
        return "Rose{" +
                "NAME='" + NAME + '\'' +
                ", COST=" + COST +
                ", type=" + type +
                '}';
    }
}
