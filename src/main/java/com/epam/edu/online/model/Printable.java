package com.epam.edu.online.model;

import java.io.IOException;

@FunctionalInterface
public interface Printable {
    void print() throws IOException;
}
