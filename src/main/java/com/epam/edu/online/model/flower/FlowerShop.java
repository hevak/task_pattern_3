package com.epam.edu.online.model.flower;

import com.epam.edu.online.Application;
import com.epam.edu.online.model.client.Subscriber;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class FlowerShop {
    private static final Logger log = LogManager.getLogger(Application.class);

    private Set<Subscriber> subscribers = new HashSet<>();
    private Set<Flower> flowers = new LinkedHashSet<>();

    public FlowerShop() {
        flowers.add(new Violet());
        flowers.add(new Rose());
    }

    public Flower createPosy(Flower flower, int count) {
        flower.setCount(count);
        return flower;
    }



    public List<Flower> getFlowerByEventType(String event) {

        List<Flower> flowersByType = new ArrayList<>();
        flowers.forEach(flower -> flower.getType().forEach(type->{
            if (type.name().toUpperCase().contains(event.trim().toUpperCase())) {
                flowersByType.add(flower);
            }
        }));

        return flowersByType;
    }
    public boolean subscribe(Subscriber subscriber) {
        return subscribers.add(subscriber);
    }
    public boolean unsubscribe(Subscriber subscriber) {
        return subscribers.remove(subscriber);
    }

    public void showAllFlowers() {
        flowers.forEach(log::info);
    }

    public void informAll(String message) {
        subscribers.forEach(subscriber -> subscriber.inform(message));
    }

    public Set<Flower> getFlowers() {
        return flowers;
    }

    public Set<Subscriber> getSubscribers() {
        return subscribers;
    }
}
