package com.epam.edu.online.view;

import com.epam.edu.online.controller.Controller;
import com.epam.edu.online.controller.ControllerImpl;
import com.epam.edu.online.model.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static final Logger log = LogManager.getLogger(MyView.class);

    private Map<String, String> menu = new LinkedHashMap<>();
    private Map<String, Printable> methodMainMenu = new LinkedHashMap<>();
    public static Scanner input = new Scanner(System.in).useLocale(Locale.US);
    public static Scanner additionalInput = new Scanner(System.in).useLocale(Locale.US);
    private Controller controller = new ControllerImpl();

    public MyView() {
        fillMenu();
        fillMethodMenu();
        printMenu();
    }

    private void printMenu() {
        menu.forEach((key, value) -> System.out.println("`" + key + "` - " + "\t" + value));
    }

    private void fillMenu() {
        menu.put("1", "add client");
        menu.put("2", "show client");
        menu.put("3", "choose client");
        menu.put("4", "set discont percentage");
        menu.put("5", "show flowers");
        menu.put("6", "show types");
        menu.put("7", "show flowers by type");
        menu.put("8", "order posy for current client");
        menu.put("9", "subscribe");
        menu.put("10", "unsubscribe");
        menu.put("11", "show subscribers");
        menu.put("12", "inform subscribers");
        menu.put("help", "print menu");
        menu.put("q", "q");
    }

    private void fillMethodMenu() {
        methodMainMenu.put("1", ()-> {
            System.out.print("Enter name of client: ");
            controller.addClient(additionalInput.nextLine());
        });
        methodMainMenu.put("2", ()-> controller.showClients());
        methodMainMenu.put("3", () -> controller.chooseClient(additionalInput));
        methodMainMenu.put("4", () -> controller.setDiscontPercentage(additionalInput));
        methodMainMenu.put("5", () -> controller.showFlowers());
        methodMainMenu.put("6", () -> controller.showTypes());
        methodMainMenu.put("7", () -> controller.showFlowersByType(additionalInput));
        methodMainMenu.put("8", () -> controller.orderPosyForCurrentClient(additionalInput));
        methodMainMenu.put("9", () -> controller.subscribe(controller.getCurrentClient()));
        methodMainMenu.put("10", () -> controller.unsubscribe(controller.getCurrentClient()));
        methodMainMenu.put("11", () -> controller.showSubscribers());
        methodMainMenu.put("12", () -> controller.informSubscribers(additionalInput.nextLine()));
        methodMainMenu.put("help", this::printMenu);
        methodMainMenu.put("q", () -> System.out.println("by"));
    }

    public void run() {
        String command = "";
        do {
            try {
                System.out.print("Enter command: ");
                command = input.nextLine().toLowerCase();
                methodMainMenu.get(command).print();
            } catch (NullPointerException e) {
                log.error("wrongcommand");
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        } while (!command.equalsIgnoreCase("q"));
    }
}
