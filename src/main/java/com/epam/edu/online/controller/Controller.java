package com.epam.edu.online.controller;

import com.epam.edu.online.model.client.Client;
import com.epam.edu.online.model.client.Subscriber;

import java.util.Scanner;

public interface Controller {
    void addClient(String name);
    void showClients();
    void chooseClient(Scanner index);
    void setDiscontPercentage(Scanner scanner);
    void showFlowers();
    void showFlowersByType(Scanner scanner);
    void showTypes();
    void orderPosyForCurrentClient(Scanner scanner);
    Client getCurrentClient();
    void subscribe(Subscriber subscriber);
    void unsubscribe(Subscriber subscriber);
    void showSubscribers();
    void informSubscribers(String message);
}
