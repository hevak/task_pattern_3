package com.epam.edu.online.controller;

import com.epam.edu.online.model.client.Client;
import com.epam.edu.online.model.client.Subscriber;
import com.epam.edu.online.model.flower.Event;
import com.epam.edu.online.model.flower.Flower;
import com.epam.edu.online.model.flower.FlowerShop;
import com.epam.edu.online.model.flower.decorator.DiscontDecorator;
import com.epam.edu.online.model.flower.decorator.TapeDecorator;
import com.epam.edu.online.view.MyView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class ControllerImpl implements Controller {
    private static final Logger log = LogManager.getLogger(MyView.class);

    private FlowerShop shop = new FlowerShop();
    private List<Client> clients = new ArrayList<>();
    private Client currentClient;

    @Override
    public void addClient(String name) {
        clients.add(new Client(name));
    }

    @Override
    public void showClients() {
        if (clients.isEmpty()) {
            log.warn("clients is empty. you need add clients");
            return;
        }
        clients.forEach(log::info);
    }

    @Override
    public void chooseClient(Scanner scanner) {
        if (clients.isEmpty()) {
            log.warn("clients is empty. you need add clients");
            return;
        }
        try {
            System.out.print("Enter index of client: ");
            this.currentClient = clients.get(scanner.nextInt());
            log.info(this.currentClient);
        } catch (IndexOutOfBoundsException e) {
            log.error("out of bounds");
        }
    }

    @Override
    public void setDiscontPercentage(Scanner scanner) {
        if (currentClient == null) {
            log.warn("first you should choose client");
            return;
        }
        int discontPercentage = 0;
        try {
            System.out.print("Enter discont percentage to client: ");
            discontPercentage = scanner.nextInt();
            currentClient.setDiscontPercentage(discontPercentage);
        } catch (InputMismatchException e){
            log.error("wrong input data type");
        }
    }

    @Override
    public void showFlowers() {
        shop.showAllFlowers();
    }

    @Override
    public void showFlowersByType(Scanner scanner) {
        System.out.print("Enter name event: ");
        shop.getFlowerByEventType(scanner.next()).forEach(log::info);
    }

    @Override
    public void showTypes() {
        Arrays.stream(Event.values()).forEach(log::info);
    }

    @Override
    public void orderPosyForCurrentClient(Scanner scanner) {
        // choose flower
        Optional<Flower> flower = getFlower(scanner);
        if (!flower.isPresent()) {
            log.info("no such element try another flower");
            return;
        }
        log.info("choosed - " + flower.get().getName());
        // choose count of flowers
        Flower posy = shop.createPosy(flower.get(), getCount(scanner));
        // decorate by tape
        posy = setTape(scanner, posy);
        // decorate by discount
        posy = new DiscontDecorator(posy, currentClient.getDiscontPercentage());
        log.info(posy.getCost() + "UAH. " + posy.toString());
    }

    @Override
    public void subscribe(Subscriber subscriber) {
        if (shop.subscribe(subscriber)) {
            log.info(subscriber.getName() + " - subscribed to updates");
        } else {
            log.error(subscriber.getName() + " - was subscribed");
        }
    }

    @Override
    public void unsubscribe(Subscriber subscriber) {
        if(shop.unsubscribe(subscriber)){
            log.info(subscriber.getName()+" - unsubscribed from updates");
        } else {
            log.error(subscriber.getName() + " - was not subscribed");
        }
    }

    @Override
    public void showSubscribers() {
        shop.getSubscribers().forEach(log::info);
    }

    @Override
    public void informSubscribers(String message) {
        shop.informAll(message);
    }

    @Override
    public Client getCurrentClient() {
        return currentClient;
    }

    private Flower setTape(Scanner scanner, Flower posy) {
        System.out.print("Need tape? (y/n): ");
        String isY = scanner.next();
        if (isY.equalsIgnoreCase("y")) {
            posy = getLength(scanner, posy);
        }
        return posy;
    }

    private int getCount(Scanner scanner) {
        int count = 0;
        System.out.print("Enter count of flower(number): ");
        try {
            count = scanner.nextInt();
        } catch (InputMismatchException e) {
            log.error(e.getMessage());
        }
        return count;
    }

    private Optional<Flower> getFlower(Scanner scanner) {
        System.out.print("Enter name of flower: ");
        String nameFlower = scanner.next();
        return shop.getFlowers()
                .stream()
                .filter(flow -> flow.getName().toUpperCase().contains(nameFlower.trim().toUpperCase()))
                .findFirst();
    }

    private Flower getLength(Scanner scanner, Flower posy) {
        try {
            System.out.print("Enter length(number): ");
            double length = scanner.nextDouble();
            posy = new TapeDecorator(posy, length);
        } catch (InputMismatchException e) {
            log.error(e.getMessage());
        }
        return posy;
    }

}
